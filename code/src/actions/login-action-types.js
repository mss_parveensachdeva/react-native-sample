import {AsyncStorage} from 'react-native';
import {NavigationActions} from 'react-navigation';
import SplashScreen from 'react-native-smart-splash-screen';
import WebApi from '../config/restClient';
import {showLoader, hideLoader} from './app-action-types';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const CLEAR_USER_DETAIL_DATA = 'CLEAR_USER_DETAIL_DATA';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const HIDE_LOGIN_ERROR_DIALOG = 'HIDE_LOGIN_ERROR_DIALOG';

export const onLogin = (userData, navigation) => {
  return (dispatch) => {
    dispatch({type: CLEAR_USER_DETAIL_DATA});
    dispatch(showLoader());
    WebApi.postApiHit('api/users/login.json', userData).then((loginResponse) => {
      if (loginResponse.success) {
        dispatch({
          type: LOGIN_SUCCESS,
          userData: loginResponse.data,
        });
        AsyncStorage.setItem('userData', JSON.stringify(loginResponse.data));
        dispatch(hideLoader());
        navigation.dispatch(
          NavigationActions.reset({
            actions: [NavigationActions.navigate({routeName: 'DrawerStack'})],
            index: 0,
            key: null,
          })
        );
      } else {
        dispatch(hideLoader());
        dispatch({
          errorMessage: loginResponse.data.message,
          type: LOGIN_FAILURE,
        });
      }
    });
  };
};
export const hideLoginErrorDialog = () => (dispatch) => {
  dispatch({type: HIDE_LOGIN_ERROR_DIALOG});
};
const dismissSplashScreen = () =>
  SplashScreen.close({
    animationType: SplashScreen.animationType.scale,
    delay: 500,
    duration: 500,
  });

export const checkLoginStatus = (navigation,
  userData
) => () => {
  WebApi.getApiHit(`api/users/loginStatus/${userData.id}.json`, null).then(
    async (user) => {
      if (user.success) {
        try {
          const stack = await AsyncStorage.getItem('initialStack');

          if (stack !== null) {
            navigation.dispatch(
              NavigationActions.reset({
                actions: [NavigationActions.navigate({routeName: stack})],
                index: 0,
                key: null,
              })
            );
          }
          dismissSplashScreen();
        } catch (error) {
          alert(error);
        }
      } else {
        alert(user.data.message);
        dismissSplashScreen();
      }
    }
  );
};
