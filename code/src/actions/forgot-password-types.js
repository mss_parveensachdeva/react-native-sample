import WebApi from '../config/restClient';
import {
  showLoader, hideLoader,
} from './app-action-types';
export const PASSWORD_RESET_SUCCESS = 'PASSWORD_RESET_SUCCESS';
export const PASSWORD_RESET_FAILURE = 'PASSWORD_RESET_FAILURE';
export const HIDE_SUCCESS_DIALOG = 'HIDE_SUCCESS_DIALOG';
export const HIDE_ERROR_DIALOG = 'HIDE_ERROR_DIALOG';

export const onResetPassword = (userEmail) => (dispatch) => {
  dispatch(showLoader());
  WebApi.postApiHit('api/users/forgotpassword.json', {email: userEmail}).then((resetPasswordResponse) => {
    if (resetPasswordResponse.success) {
      dispatch({
        successMessage: resetPasswordResponse.data.result,
        type: PASSWORD_RESET_SUCCESS,
      });
      dispatch(hideLoader());
    } else {
      dispatch(hideLoader());
      dispatch({
        errorMessage: resetPasswordResponse.data.message,
        type: PASSWORD_RESET_FAILURE,
      });
    }
  });
};

export const hideSuccessDialog = () => (dispatch) => {
  dispatch({type: HIDE_SUCCESS_DIALOG});
};
export const hideErrorDialog = () => (dispatch) => {
  dispatch({type: HIDE_ERROR_DIALOG});
};
