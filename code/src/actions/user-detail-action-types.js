import {AsyncStorage} from 'react-native';
import {NavigationActions} from 'react-navigation';
import WebApi from '../config/restClient';
import {showLoader, hideLoader} from './app-action-types';
export const FETCH_USER_DATA = 'FETCH_USER_DATA';
export const FETCH_PROFILE_SUCCESS = 'FETCH_PROFILE_SUCCESS';

export const getUserProfile = (userData, navigation) => {
  return (dispatch) => {
    // dispatch({payload: userData, type: FETCH_USER_DATA});
    WebApi.getApiHit(
      `api/users/getProfile/${userData.id}.json`,
      null,
      userData.token
    ).then((profileResponse) => {
      if (profileResponse.success) {
        dispatch({
          profileData: profileResponse.data,
          type: FETCH_PROFILE_SUCCESS,
          unread_msg: profileResponse.data.unread_msgs,
          userData: userData,
        });
      } else {
        // alert(profileResponse.data.message);
        AsyncStorage.removeItem('initialStack');
        AsyncStorage.removeItem('userData');
        navigation.dispatch(
          NavigationActions.reset({
            actions: [NavigationActions.navigate({routeName: 'AppLoginStack'})],
            index: 0,
            key: null,
          })
        );
      }
    });
  };
};

export const onLogOut = (userId, token, navigation) => (dispatch) => {
  dispatch(showLoader());
  WebApi.postApiHitWithHeader(
    'api/users/logout.json',
    {user_id: userId},
    token
  ).then((logoutResponse) => {
    if (logoutResponse.success) {
      dispatch(hideLoader());
      AsyncStorage.removeItem('initialStack');
      AsyncStorage.removeItem('userData');
      navigation.dispatch(
        NavigationActions.reset({
          actions: [NavigationActions.navigate({routeName: 'AppLoginStack'})],
          index: 0,
          key: null,
        })
      );
    } else {
      alert(logoutResponse.data.message);
      dispatch(hideLoader());
      AsyncStorage.removeItem('initialStack');
      AsyncStorage.removeItem('userData');
      navigation.dispatch(
        NavigationActions.reset({
          actions: [NavigationActions.navigate({routeName: 'AppLoginStack'})],
          index: 0,
          key: null,
        })
      );
    }
  });
};
