import WebApi from '../config/restClient';
import {showLoader, hideLoader} from './app-action-types';
export const SIGN_UP_FAILURE = 'SIGN_UP_FAILURE';
export const HIDE_SIGN_UP_SUCCESS_DIALOG = 'HIDE_SIGN_UP_SUCCESS_DIALOG';
export const HIDE_SIGN_UP_ERROR_DIALOG = 'HIDE_SIGN_UP_ERROR_DIALOG';
export const CLEAR_SIGN_UP_FIELD_DATA = 'CLEAR_SIGN_UP_FIELD_DATA';
export const SIGN_UP_SUCCESS = 'SIGN_UP_SUCCESS';

export const onSignUp = (userData) => {
  return (dispatch) => {
    dispatch(showLoader());
    WebApi.postApiHit('api/users/add.json', userData).then((signUpResponse) => {
      if (signUpResponse.success) {
        dispatch({signUpResponse: signUpResponse, type: SIGN_UP_SUCCESS});
        dispatch(hideLoader());
      } else {
        dispatch({
          errorMessage: signUpResponse.data.message,
          type: SIGN_UP_FAILURE,
        });
        dispatch(hideLoader());
      }
    });
  };
};

export const hideSignUpSuccessDialog = () => {
  return (dispatch) => {
    dispatch({type: HIDE_SIGN_UP_SUCCESS_DIALOG});
  };
};

export const hideSignUpErrorDialog = () => {
  return (dispatch) => {
    dispatch({type: HIDE_SIGN_UP_ERROR_DIALOG});
  };
};

export const clearSignUpFieldsData = () => {
  return (dispatch) => {
    dispatch({type: CLEAR_SIGN_UP_FIELD_DATA});
  };
};
