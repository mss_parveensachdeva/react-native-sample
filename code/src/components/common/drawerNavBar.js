import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Platform,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import Metrics from '../../config/metrics';
import {Color} from '../../constants/colors';

export const DrawerNavBar = (props) => {
  const {navigation,title} = props;

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.drawerNavBar}
        onPress={() => {
          navigation.state.index === 0
            ? navigation.navigate('DrawerOpen')
            : navigation.navigate('DrawerClose');
        }}
      >
        <Icon size={30} color="white" name="menu" />
      </TouchableOpacity>
      <View style={styles.navBarContainer}>
        <Image
          style={styles.appIcon}
          source={require('../../assets/images/spotter_icon.png')}
        />
        <Text style={styles.titleText}>{title}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  addButton: {marginRight: 10, padding: 5},
  appIcon: {height: 35, width: 35},
  container: {
    alignItems: 'center',
    backgroundColor: Color.colorBlack,
    flexDirection: 'row',
    height: Platform.OS === 'android' ? 55 : 65,
    justifyContent: 'space-between',
    paddingTop: Platform.OS === 'android' ? 0 : 25,
    width: Metrics.DEVICE_WIDTH,
  },
  dotStyle: {
    backgroundColor: Color.colorPrimaryDark,
    borderRadius: 5,
    height: 10,
    position: 'absolute',
    right: 5,
    top: 10,
    width: 10,
  },
  drawerNavBar: {flexWrap: 'wrap', padding: 5},
  navBarContainer: {
    alignItems: 'center',
    alignSelf: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: Platform.OS === 'android' ? 0 : 5,
    paddingRight: 35,
  },
  titleText: {
    color: '#4A8EDF',
    fontSize: 19,
    fontWeight: 'bold',
    marginLeft: 10,
  },
});
