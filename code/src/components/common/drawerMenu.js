import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import FontIcon from 'react-native-vector-icons/FontAwesome';
import {LoginManager} from 'react-native-fbsdk';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import LinearGradient from 'react-native-linear-gradient';
import {Avatar} from 'react-native-elements';
import {NavigationActions} from 'react-navigation';
import {
  ScrollView,
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Metrics from '../../config/metrics';
import {onLogOut} from '../../actions/user-detail-action-types';
import {language} from '../../config/localization';

const navigateToScreen = (route,navigation) => {

  const navigateAction = NavigationActions.navigate({
    routeName: route,
  });

  navigation.dispatch(navigateAction);
};

const DrawerMenu = (props) => {
  const {userDetail,onPressLogOut,navigation} = props;

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.subContainer}>
          <View style={styles.drawerMenuView}>
            <Avatar
              large
              rounded
              source={
                userDetail.profileData == null
                  ? require('../../assets/images/user_image.png')
                  : userDetail.profileData.imageUrl === ''
                    ? require('../../assets/images/user_image.png')
                    : {uri: userDetail.profileData.imageUrl}
              }
              activeOpacity={0.7}
            />
            <Text numberOfLines={1} style={styles.userNameText}>
              {userDetail.profileData != null
                ? `${userDetail.profileData.firstname} ${userDetail.profileData.lastname}`
                : ''}
            </Text>
          </View>
          <TouchableOpacity
            style={styles.homeView}
            onPress={() => navigateToScreen('DrawerLocationScreen',navigation)}
          >
            <View style={styles.inputStyle}>
              <EntypoIcon name="home" size={25} color="grey" />
              <View style={styles.homeContainer}>
                <Text style={styles.homeText}>{'Home'}</Text>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.homeView}
            onPress={() => navigateToScreen('ProfileScreen',navigation)}
          >
            <View style={styles.inputStyle}>
              <FontIcon name="user-circle-o" size={25} color="grey" />
              <View style={styles.homeContainer}>
                <Text style={styles.homeText}>{'Profile'}</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
      <TouchableOpacity
        style={styles.touchableStyle}
        onPress={() => {
          LoginManager.logOut();
          onPressLogOut(
            userDetail.userData.id,
            userDetail.userData.token,
            navigation
          );
        }}
      >
        <LinearGradient
          colors={['#eda374', '#cb6133', '#cb6133']}
          style={styles.linearGradient}
        >
          <View style={styles.subContainerView}>
            <Text style={styles.buttonText}>{language.en.logOut}</Text>
          </View>
        </LinearGradient>
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  buttonText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft: 20,
    marginRight: 20,
  },
  container: {
    flex: 1,
    height: Metrics.DEVICE_HEIGHT,
  },
  drawerMenuView: {
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    marginBottom: 20,
    marginTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
  },
  emptyView: {height: 20, width: 20},
  frameView: {
    height: Metrics.DEVICE_HEIGHT,
    position: 'absolute',
    width: Metrics.DEVICE_WIDTH,
  },
  homeContainer: {
    alignItems: 'center',
    flexDirection: 'column',
    flexWrap: 'wrap',
    marginLeft: 30,
  },
  homeText: {
    color: 'black',
    fontSize: 16,
    fontWeight: 'bold',
  },
  homeView: {
    flex: 1,
    height: 50,
  },
  inputStyle: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    marginTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
  },
  linearGradient: {
    flex: 1,
    height: 45,
  },
  messageCountBg: {
    alignItems: 'center',
    height: 30,
    justifyContent: 'center',
    marginLeft: 20,
    width: 30,
  },
  subContainer: {flex: 1, flexDirection: 'column'},
  subContainerView: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 8,
  },
  touchableStyle: {
    bottom: 0,
    height: 45,
    position:'absolute',
    width: Metrics.DEVICE_WIDTH * 0.7,
  },
  unreadMessageCountText: {
    color: 'white',
    fontSize: 12,
    height: 30,
    paddingTop: 7,
    textAlign: 'center',
    width: 30,
  },
  userNameText: {
    color: 'black',
    flex: 1,
    fontSize: 16,
    fontWeight: 'bold',
    marginLeft: 20,
    textAlign: 'left',
  },
});

DrawerMenu.propTypes = {
  navigation: PropTypes.object,
};
const mapStateToProps = (state) => {
  return {
    userDetail: state.userDetail,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onPressLogOut: bindActionCreators(onLogOut, dispatch),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DrawerMenu);
