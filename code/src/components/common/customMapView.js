import React from 'react';
import {StyleSheet} from 'react-native';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';

export const CustomMapView = () => {
  return (
    <MapView.Animated
      ref={(map) => (this.map = map)}
      provider={PROVIDER_GOOGLE}
      style={styles.mapView}
      showsUserLocation={true}
      followUserLocation={true}
    />
  );
};

const styles = StyleSheet.create({
  mapView: {flex: 1},
});
