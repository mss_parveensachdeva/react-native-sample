import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Platform,
  Image,
} from 'react-native';
import Metrics from '../../config/metrics';
import {Color} from '../../constants/colors';

export const CustomNavBar = (props) => {
  const {onBackPress,title} = props;

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.backButtonContainer}
        onPress={onBackPress}
      >
        <Text style={styles.backText}>Back</Text>
      </TouchableOpacity>
      <View style={styles.appIconView}>
        <Image
          style={styles.appIconImage}
          source={require('../../assets/images/spotter_icon.png')}
        />
        <Text style={styles.navBarTitle}>{title}</Text>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  appIconImage: {height: 35, width: 35},
  appIconView: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: Platform.OS === 'android' ? 0 : 5,
    marginRight: 80,
  },
  backButtonContainer: {
    alignItems: 'center',
    flexWrap: 'wrap',
    justifyContent: 'center',
    padding: 5,
  },
  backText: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  container: {
    alignItems: 'center',
    backgroundColor: Color.colorBlack,
    flexDirection: 'row',
    height: Platform.OS === 'android' ? 55 : 65,
    justifyContent: 'space-between',
    paddingLeft: 10,
    paddingTop: Platform.OS === 'android' ? 0 : 25,
    width: Metrics.DEVICE_WIDTH,
  },
  navBarTitle: {
    color: '#4A8EDF',
    fontSize: 19,
    fontWeight: 'bold',
    marginLeft: 10,
  },
});
