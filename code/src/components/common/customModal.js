import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Modal,
  TouchableOpacity,
  Image,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {language} from '../../config/localization';
import Metrics from '../../config/metrics';

export const CustomModal = (props) => {
  const {showDialog,message,onPressOkButton} = props;

  return (
    <View style={styles.container}>
      <Modal
        transparent={true}
        visible={showDialog}
        animationType={'fade'}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}
      >
        <View style={styles.modalSubContainer}>
          <View style={styles.modalView}>
            <Image
              style={styles.appIconImage}
              resizeMode={'contain'}
              source={require('../../assets/images/app_icon.png')}
            />
            <Text style={styles.errorMessage}>{message}</Text>

            <TouchableOpacity
              style={styles.touchableStyle}
              onPress={onPressOkButton}
            >
              <LinearGradient
                colors={['#eda374', '#cb6133', '#cb6133']}
                style={styles.linearGradient}
              >
                <View style={styles.okButtonView}>
                  <Text style={styles.okText}>{language.en.ok}</Text>
                </View>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  appIconImage: {
    height: Metrics.DEVICE_WIDTH * 0.2,
    width: Metrics.DEVICE_WIDTH * 0.2,
  },
  container: {
    alignItems: 'center',
    backgroundColor: 'red',
    flex: 1,
    justifyContent: 'center',
    position: 'absolute',
  },
  errorMessage: {
    color: 'black',
    fontSize: 15,
    marginBottom: 20,
    marginTop: 20,
    textAlign: 'center',
  },
  linearGradient: {
    borderRadius: 20,
    flex: 1,
    shadowColor: '#af625f',
    shadowOffset: {
      height: 0,
      width: 0,
    },
    shadowOpacity: 1.0,
    shadowRadius: 20,
  },
  modalSubContainer: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  modalView: {
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 20,
    flexDirection: 'column',
    flexWrap: 'wrap',
    justifyContent: 'center',
    padding: 20,
    width: Metrics.DEVICE_WIDTH * 0.9,
  },
  okButtonView: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 8,
  },
  okText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft: 20,
    marginRight: 20,
  },
  touchableStyle: {
    borderRadius: 20,
    height: 45,
    marginTop: 30,
    shadowColor: '#af625f',
    shadowOffset: {
      height: 0,
      width: 0,
    },
    shadowOpacity: 1.0,
    shadowRadius: 20,
    width: Metrics.DEVICE_WIDTH * 0.6,
  },
});
