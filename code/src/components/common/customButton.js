import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import LinearGradient from 'react-native-linear-gradient';

const renderIcon = (showIcon) => {
  if (showIcon) {
    return <Icon size={20} name="check" color="white" />;
  }
  return null;
};

export const CustomButton = (props) => {
  const {onPress,style,btnText,showIcon} = props;

  return (
    <TouchableOpacity
      style={[styles.touchableStyle, style]}
      onPress={onPress}
    >
      <LinearGradient
        colors={['#eda374', '#cb6133', '#cb6133']}
        style={styles.linearGradient}
      >
        <View style={styles.subContainerView}>
          {renderIcon(showIcon)}
          <Text style={styles.buttonText}>{btnText}</Text>
        </View>
      </LinearGradient>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  buttonText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft: 20,
    marginRight: 20,
  },
  container: {
    alignItems: 'center',
    backgroundColor: '#2c3e50',
    flex: 1,
    justifyContent: 'center',
  },
  linearGradient: {
    borderRadius: 20,
    height: 45,
    shadowColor: '#af625f',
    shadowOffset: {
      height: 0,
      width: 0,
    },
    shadowOpacity: 1.0,
    shadowRadius: 20,
  },
  subContainerView: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 8,
  },
  touchableStyle: {
    borderRadius: 20,
    height: 45,
    shadowColor: '#af625f',
    shadowOffset: {
      height: 0,
      width: 0,
    },
    shadowOpacity: 1.0,
    shadowRadius: 20,
  },
});
