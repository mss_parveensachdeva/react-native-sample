export const Color = {
  colorBlack: '#2B2B2B',
  colorPrimary: '#eda374',
  colorPrimaryDark: '#cb6133',
};
