import React from 'react';
import ValidationComponent from 'react-native-form-validator';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  Platform,
  TextInput,
  ImageBackground,
  AsyncStorage,
  Keyboard,
} from 'react-native';
import {bindActionCreators} from 'redux';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import Dialog from 'react-native-dialog';
import {connect} from 'react-redux';
import {
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager,
} from 'react-native-fbsdk';
import SplashScreen from 'react-native-smart-splash-screen';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Metrics from '../../config/metrics';
import {Color} from '../../constants/colors';
import {CustomButton} from '../../components/common/customButton';
import {CustomModal} from '../../components/common/customModal';
import {language} from '../../config/localization';
import {
  onLogin,
  hideLoginErrorDialog,
  checkLoginStatus,
} from '../../actions/login-action-types';

class LoginScreen extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = {
      dialogVisible: false,
      email: '',
      fbEmail: '',
      fbResult: null,
      getStarted: true,
      password: '',
      showModal: false,
    };
  }

  static navigationOptions = {
    header: null,
  };

  componentDidMount = async () => {
    const {checkLoggedInStatus,navigation} = this.props;

    try {
      const startedOrLogin = await AsyncStorage.getItem('showStarted');

      if (startedOrLogin != null) {
        this.setState({getStarted: JSON.parse(startedOrLogin)});
      }
    } catch (error) {
      alert(error.toString());
    }
    try {
      const userData = await AsyncStorage.getItem('userData');

      if (userData != null) {
        checkLoggedInStatus(navigation,JSON.parse(userData));
      } else {
        SplashScreen.close({
          animationType: SplashScreen.animationType.scale,
          delay: 500,
          duration: 500,
        });
      }
    } catch (error) {
      alert(error.toString());
    }
  };

  handleCancel = () => {
    LoginManager.logOut();
    this.setState({dialogVisible: false});
  };

  checkValidEmail(email) {
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (reg.test(email) === false) {
      return false;
    } else {
      return true;
    }
  }

  handleUpdateEmail = () => {
    const {fbResult, fbEmail} = this.state;
    const {onLoginUser,navigation} = this.props;

    if (this.checkValidEmail(fbEmail) && fbEmail !== '') {
      Keyboard.dismiss();
      this.setState({dialogVisible: false});
      const userData = {
        device_token: '',
        device_type: Platform.OS === 'android' ? 1 : 2,
        email: fbEmail,
        first_name: fbResult.first_name,
        id: fbResult.id,
        imageUrl: fbResult.picture.data.url,
        last_name: fbResult.last_name,
      };

      onLoginUser(userData, navigation);
    } else {
      alert('Please enter valid email address.');
    }
  };

  onPressSignIn() {
    const {onLoginUser,navigation} = this.props;

    const validate = this.validate({
      email: {email: true, required: true},
      password: {maxlength: 24, minlength: 3, required: true},
    });

    if (validate) {
      const userData = {
        device_token: '',
        device_type: Platform.OS === 'android' ? 1 : 2,
        email: this.state.email,
        password: this.state.password,
      };

      onLoginUser(userData, navigation);
    } else {
      this.setState({showModal: true});
    }
  }

  renderGetStartedOrLoginView() {
    const {onLoginUser,navigation,onHideLoginErrorDialog,login} = this.props;

    if (this.state.getStarted) {
      return (
        <View style={styles.getStartedContainer}>
          <Image
            style={styles.getStartedImage}
            resizeMode={'cover'}
            source={require('../../assets/images/get_start_bg.png')}
          />
          <Text style={styles.welcomeText}>{language.en.welcomeTo}</Text>
          <Text style={styles.spotterText}>{language.en.spotter}</Text>
          <CustomButton
            style={styles.getStartedButton}
            showIcon={true}
            btnText={language.en.getStarted}
            onPress={() => {
              this.setState({getStarted: false});
              AsyncStorage.setItem('showStarted', JSON.stringify(false));
            }}
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <ImageBackground
          style={styles.blurBackgroundImage}
          resizeMode={'stretch'}
          source={require('../../assets/images/screen_blur_bg.png')}
        >
          <ScrollView
            keyboardShouldPersistTaps={'handled'}
            showsVerticalScrollIndicator={false}
            style={styles.scrollView}
          >
            <View style={styles.loginView}>
              <View style={styles.loginContainer}>
                <Image
                  style={styles.loginImage}
                  resizeMode={'contain'}
                  source={require('../../assets/images/ic_login.png')}
                />
                <LoginButton
                  readPermissions={['email', 'public_profile']}
                  onLoginFinished={(error, result) => {
                    if (error) {
                      alert('login has error: ' + result.error);
                    } else if (result.isCancelled) {
                      alert('login is cancelled.');
                    } else {
                      AccessToken.getCurrentAccessToken().then((data) => {
                        const responseInfoCallback = (callBackError, callBackResult) => {
                          if (callBackError) {
                            alert('Error fetching data: ' + callBackError.toString());
                          } else if ('email' in callBackResult) {
                            const userData = {
                              device_token: '',
                              device_type: Platform.OS === 'android' ? 1 : 2,
                              email: callBackResult.email,
                              first_name: callBackResult.first_name,
                              id: callBackResult.id,
                              imageUrl: callBackResult.picture.data.url,
                              last_name: callBackResult.last_name,
                            };

                            onLoginUser(userData,navigation);
                          } else {
                            this.setState({
                              dialogVisible: true,
                              fbResult: callBackResult,
                            });
                          }
                        };

                        const infoRequest = new GraphRequest(
                          '/me?fields=email,first_name,last_name,picture',
                          null,
                          responseInfoCallback
                        );

                        // Start the graph request.
                        new GraphRequestManager()
                          .addRequest(infoRequest)
                          .start();
                      });
                    }
                  }}
                  onLogoutFinished={() => alert('logout')}
                />
              </View>
              <View style={styles.fieldsContainer}>
                <View style={styles.fieldsSubContainer}>
                  <View style={styles.fieldInput}>
                    <Icon name="email" size={25} color="white" />
                    <TextInput
                      style={styles.emailInput}
                      ref={(email) => {this.email = email;}}
                      placeholder={'Email'}
                      value={this.state.email}
                      keyboardType={'email-address'}
                      selectionColor={'white'}
                      returnKeyType={'next'}
                      multiline={false}
                      placeholderTextColor={'white'}
                      onSubmitEditing={() => this.password.focus()}
                      onChangeText={(email) => this.setState({email})}
                      underlineColorAndroid={'transparent'}
                    />
                  </View>
                  <View style={styles.bottomBorder} />
                  <View style={styles.fieldInput}>
                    <Icon name="lock" size={25} color="white" />
                    <TextInput
                      style={styles.passwordInput}
                      ref={(password) => {this.password = password;}}
                      value={this.state.password}
                      placeholder={'Password'}
                      keyboardType={'default'}
                      secureTextEntry={true}
                      multiline={false}
                      selectionColor={'white'}
                      returnKeyType={'done'}
                      placeholderTextColor={'white'}
                      onChangeText={(password) => this.setState({password})}
                      underlineColorAndroid={'transparent'}
                    />
                  </View>
                  <View style={styles.bottomBorder} />
                  <TouchableOpacity
                    style={styles.forgotPasswordView}
                    onPress={() =>
                      navigation.navigate('ForgotPasswordScreen')
                    }
                  >
                    <Text style={styles.forgotPasswordText}>
                      {language.en.forgotPassword}
                    </Text>
                  </TouchableOpacity>
                </View>

                <CustomButton
                  style={styles.loginButton}
                  showIcon={false}
                  btnText={language.en.signIn}
                  onPress={() => {
                    this.onPressSignIn();
                  }}
                />
                <View style={styles.hairLineView} />
                <View style={styles.didNotAccountView}>
                  <Text style={styles.didNotText}>
                    {language.en.didNotAccount}
                  </Text>
                  <TouchableOpacity
                    style={styles.signUpTextContainer}
                    onPress={() =>
                      navigation.navigate('SignUpScreen')
                    }
                  >
                    <Text style={styles.signUpText}>{language.en.signUp}</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <Dialog.Container visible={this.state.dialogVisible}>
                <Dialog.Title>{'Enter Email'}</Dialog.Title>
                <Dialog.Description>
                  {' Do you need to enter valid email address to this account.'}
                </Dialog.Description>
                <Dialog.Input
                  placeholder={'Email Address'}
                  onChangeText={(email) => this.setState({fbEmail: email})}
                />
                <Dialog.Button label="Cancel" onPress={this.handleCancel} />
                <Dialog.Button
                  label="Update"
                  onPress={this.handleUpdateEmail}
                />
              </Dialog.Container>
            </View>
          </ScrollView>
        </ImageBackground>
        {Platform.OS === 'ios' ? <KeyboardSpacer /> : null}
        <CustomModal
          onPressOkButton={() =>
            this.setState({
              showModal: false,
            })
          }
          showDialog={this.state.showModal}
          message={this.getErrorMessages()}
        />

        <CustomModal
          onPressOkButton={() => onHideLoginErrorDialog()}
          showDialog={login.showErrorDialog}
          message={login.errorMessage}
        />
      </View>
    );
  }

  render() {
    return this.renderGetStartedOrLoginView();
  }
}

const styles = StyleSheet.create({
  blurBackgroundImage: {
    flex: 1,
  },
  bottomBorder: {
    backgroundColor: 'white',
    height: 0.5,
    width: Metrics.DEVICE_WIDTH - 40,
  },
  container: {
    flex: 1,
  },
  didNotAccountView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 15,
  },
  didNotText: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 15,
  },
  emailInput: {
    color: 'white',
    flex: 1,
    height: 45,
    marginTop: 15,
    marginLeft: 10,
  },
  facebookButton: {
    alignItems: 'center',
    height: 30,
    justifyContent: 'center',
    width: 175,
  },
  facebookButtonContent: {
    alignItems: 'center',
    height: 30,
    justifyContent: 'center',
    width: 175,
  },
  fbButton: {height: 45, width: Metrics.DEVICE_WIDTH * 0.5},
  fieldInput: {
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    paddingLeft: 10,
    paddingRight: 10,
  },
  fieldsContainer: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    height: Metrics.DEVICE_HEIGHT * 0.5,
    justifyContent: 'center',
  },
  fieldsSubContainer: {
    flexWrap: 'wrap',
    width: Metrics.DEVICE_WIDTH * 0.9,
  },
  forgotPasswordText: {
    alignItems: 'center',
    color: 'white',
    fontSize: 15,
  },
  forgotPasswordView: {
    alignSelf: 'flex-end',
    height: 50,
    justifyContent: 'center',
    padding: 4,
  },
  getStartedButton: {
    bottom: Metrics.DEVICE_HEIGHT * 0.15,
    flexWrap: 'wrap',
    position: 'absolute',
    width: Metrics.DEVICE_WIDTH * 0.55,
  },
  getStartedContainer: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  getStartedImage: {
    height: Metrics.DEVICE_HEIGHT,
    width: Metrics.DEVICE_WIDTH,
  },
  hairLineView: {
    backgroundColor: 'white',
    height: 0.5,
    marginTop: 20,
    width: Metrics.DEVICE_WIDTH * 0.8,
  },
  loginButton: {
    flexWrap: 'wrap',
    width: Metrics.DEVICE_WIDTH * 0.65,
  },
  loginContainer: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    height: Metrics.DEVICE_HEIGHT * 0.5,
    justifyContent: 'center',
  },
  loginImage: {
    height: Metrics.DEVICE_WIDTH * 0.4,
    marginBottom: 20,
    width: Metrics.DEVICE_WIDTH * 0.4,
  },
  loginView: {
    alignItems: 'center',
    backgroundColor: 'transparent',
    flex: 1,
    justifyContent: 'center',
  },
  passwordInput: {
    color: 'white',
    flex: 1,
    paddingTop: 15,
    height: 45,
    marginLeft: 10,
  },
  scrollView: {flex: 1},
  signUpText: {
    color: Color.colorPrimary,
    fontSize: 15,
    marginLeft: 5,
  },
  signUpTextContainer: {alignSelf: 'center'},
  spotterText: {
    color: 'white',
    flexWrap: 'wrap',
    fontSize: 55,
    fontWeight: 'bold',
    position: 'absolute',
    top: Metrics.DEVICE_HEIGHT * 0.3,
  },
  welcomeText: {
    color: 'white',
    flexWrap: 'wrap',
    fontSize: 19,
    fontWeight: 'bold',
    position: 'absolute',
    top: Metrics.DEVICE_HEIGHT * 0.15,
  },
});

const mapStateToProps = (state) => {
  return {
    login: state.login,
  };
};

const mapDispatchToProps = (dispatch) => ({
  checkLoggedInStatus: bindActionCreators(checkLoginStatus, dispatch),
  onHideLoginErrorDialog: bindActionCreators(hideLoginErrorDialog, dispatch),
  onLoginUser: bindActionCreators(onLogin, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginScreen);
