import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Platform,
  Modal,
  TextInput,
  ImageBackground,
  NativeModules,
} from 'react-native';
import {bindActionCreators} from 'redux';
import Icon from 'react-native-vector-icons/Ionicons';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import ValidationComponent from 'react-native-form-validator';
import MaterialCommIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {Avatar} from 'react-native-elements';
import {connect} from 'react-redux';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import FoundationIcon from 'react-native-vector-icons/Foundation';
import Metrics from '../../config/metrics';
import {CustomButton} from '../../components/common/customButton';
import {CustomModal} from '../../components/common/customModal';
import {language} from '../../config/localization';
import {
  onSignUp,
  hideSignUpSuccessDialog,
  hideSignUpErrorDialog,
  clearSignUpFieldsData,
} from '../../actions/sign-up-action-types';
const ImagePicker = NativeModules.ImageCropPicker;

import {Color} from '../../constants/colors';

class SignUpScreen extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = {
      customMessage: false,
      email: '',
      firstName: '',
      gender: 'Specify Gender',
      genderModal: false,
      image: null,
      lastName: '',
      password: '',
      selectedIndex: null,
      showModal: false,
    };
  }

  static navigationOptions = {
    header: null,
  };

  onPressSignUp() {
    const validate = this.validate({
      email: {email: true, required: true},
      firstName: {maxlength: 24, required: true},
      lastName: {maxlength: 24, required: true},
      password: {maxlength: 24, minlength: 3,  required: true},
    });

    if (validate) {
      const {login,onClickSignUp} = this.props;

      if (this.state.gender !== 'Specify Gender') {
        const userDataObject = {
          address: login.currentAddress,
          email: this.state.email,
          firstname: this.state.firstName,
          gender: this.state.gender,
          imageUrl: this.state.image,
          lastname: this.state.lastName,
          password: this.state.password,
          workout_place_id: '10',
          workout_place_name: 'Ozi Gym',
        };

        onClickSignUp(userDataObject);
      } else {
        this.setState({customMessage: true, showModal: true});
      }
    } else {
      this.setState({showModal: true});
    }
  }

  pickSingle() {
    ImagePicker.openPicker({
      compressImageMaxHeight: 480,
      compressImageMaxWidth: 640,
      compressImageQuality: 0.5,
      compressVideoPreset: 'MediumQuality',
      cropperCircleOverlay: true,
      cropping: true,
      height: 300,
      includeExif: true,
      width: 300,
    })
      .then((image) => {
        this.setState({
          image: {
            name:
              Platform.OS === 'ios'
                ? image.filename
                : image.path.substring(
                  image.path.lastIndexOf('/') + 1,
                  image.path.length
                ),
            type: image.mime,
            uri: image.path,
          },
        });
      })
      .catch((e) => {
        console.log(e);
        alert(e.message ? e.message : e);
      });
  }

  onSelect(index, value) {
    this.setState({gender: value, genderModal: false, selectedIndex: index});
  }

  componentWillUnmount() {
    const {clearFieldsData} = this.props;

    clearFieldsData();
  }

  render() {
    const {signUp,onHideSignUpErrorDialog,onHideSignUpSuccessDialog,navigation} = this.props;

    return (
      <View style={styles.container}>
        <ImageBackground
          style={styles.imageBackground}
          resizeMode={'stretch'}
          source={require('../../assets/images/screen_blur_bg.png')}
        >
          <ScrollView
            keyboardShouldPersistTaps={'handled'}
            showsVerticalScrollIndicator={false}
            style={styles.scrollView}
          >
            <View style={styles.choosePicView}>
              <View style={styles.choosePicContainer}>
                <Avatar
                  xlarge
                  rounded
                  source={
                    this.state.image == null
                      ? require('../../assets/images/user_image.png')
                      : this.state.image
                  }
                  onPress={() => console.log('Works!')}
                  activeOpacity={0.7}
                />
                <TouchableOpacity
                  style={styles.addButtonView}
                  onPress={() => this.pickSingle()}
                >
                  <Icon
                    size={30}
                    color="grey"
                    name="ios-add"
                    style={styles.addIcon}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.fieldsView}>
              <View style={styles.inputStyle}>
                <EntypoIcon name="edit" size={25} color="white" />
                <TextInput
                  style={styles.fieldInput}
                  ref={(firstName) => {
                    this.firstName = firstName;
                  }}
                  placeholder={'First Name'}
                  value={this.state.firstName}
                  keyboardType={'default'}
                  selectionColor={'white'}
                  returnKeyType={'next'}
                  placeholderTextColor={'white'}
                  maxLength={42}
                  onSubmitEditing={() => this.lastName.focus()}
                  onChangeText={(firstName) => this.setState({firstName})}
                  underlineColorAndroid={'transparent'}
                />
              </View>
              <View style={styles.bottomBorder} />
              <View style={styles.inputStyle}>
                <EntypoIcon name="edit" size={25} color="white" />
                <TextInput
                  style={styles.fieldInput}
                  ref={(lastName) => {
                    this.lastName = lastName;
                  }}
                  placeholder={'Last Name'}
                  value={this.state.lastName}
                  keyboardType={'default'}
                  selectionColor={'white'}
                  returnKeyType={'next'}
                  placeholderTextColor={'white'}
                  maxLength={42}
                  onSubmitEditing={() => this.email.focus()}
                  onChangeText={(lastName) => this.setState({lastName})}
                  underlineColorAndroid={'transparent'}
                />
              </View>
              <View style={styles.bottomBorder} />
              <View style={styles.inputStyle}>
                <MaterialIcon name="email" size={25} color="white" />
                <TextInput
                  style={styles.fieldInput}
                  ref={(email) => {
                    this.email = email;
                  }}
                  placeholder={'Your Email'}
                  value={this.state.email}
                  keyboardType={'email-address'}
                  selectionColor={'white'}
                  returnKeyType={'next'}
                  placeholderTextColor={'white'}
                  onSubmitEditing={() => this.password.focus()}
                  onChangeText={(email) => this.setState({email})}
                  underlineColorAndroid={'transparent'}
                />
              </View>
              <View style={styles.bottomBorder} />
              <View style={styles.inputStyle}>
                <MaterialIcon name="lock" size={25} color="white" />
                <TextInput
                  style={styles.fieldInput}
                  ref={(password) => {
                    this.password = password;
                  }}
                  placeholder={'Password'}
                  value={this.state.password}
                  keyboardType={'default'}
                  secureTextEntry={true}
                  selectionColor={'white'}
                  returnKeyType={'done'}
                  placeholderTextColor={'white'}
                  onChangeText={(password) => this.setState({password})}
                  underlineColorAndroid={'transparent'}
                />
              </View>
              <View style={styles.bottomBorder} />
              <View style={styles.inputStyle}>
                <FoundationIcon name="male-female" size={25} color="white" />
                <TouchableOpacity
                  style={styles.chooseGenderView}
                  onPress={() =>
                    this.setState({genderModal: !this.state.genderModal})
                  }
                >
                  <View style={styles.genderContainer}>
                    <Text style={styles.selectGenderText}>
                      {this.state.gender}
                    </Text>
                    <Icon
                      name="ios-arrow-forward-outline"
                      size={25}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={styles.bottomBorder} />
              </View>
              <View style={styles.inputStyle}>
                <MaterialIcon name="location-on" size={25} color="white" />
                <View style={styles.currentAddressView}>
                  <TouchableOpacity style={styles.currentAddressContainer}>
                    <View style={styles.currentAddressSubContainer}>
                      <Text style={styles.currentAddressText}>
                        {'Sahibzada Ajit Singh Nagar, Punjab.'}
                      </Text>
                      <Icon
                        name="ios-arrow-forward-outline"
                        size={25}
                        color="white"
                      />
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={styles.bottomBorder} />
              </View>
              <View style={styles.inputStyle}>
                <MaterialCommIcon name="dumbbell" size={25} color="white" />
                <View style={styles.chooseWorkoutView}>
                  <TouchableOpacity
                    style={styles.workoutContainer}
                  >
                    <View style={styles.workoutSubContainer}>
                      <Text style={styles.workoutText}>
                        {'Ozi Gym'}
                      </Text>
                      <Icon
                        name="ios-arrow-forward-outline"
                        size={25}
                        color="white"
                      />
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={styles.bottomBorder} />
              </View>
              <CustomButton
                style={styles.signUpButton}
                showIcon={false}
                btnText={language.en.signUp}
                onPress={() => {
                  this.onPressSignUp();
                }}
              />
              <View style={styles.alreadyHaveAccountView}>
                <Text style={styles.alreadyHaveText}>
                  {language.en.alreadyHaveAccount}
                </Text>
                <TouchableOpacity
                  style={styles.signInText}
                  onPress={() => navigation.goBack()}
                >
                  <Text style={styles.signIn}>{language.en.signIn}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
          {Platform.OS === 'ios' ? <KeyboardSpacer /> : null}
          <Modal
            transparent={true}
            visible={this.state.genderModal}
            animationType={'fade'}
            onRequestClose={() => {
              alert('Modal has been closed.');
            }}
          >
            <View
              style={styles.genderModalView}
              onPress={() => {
                this.setState({genderModal: false});
              }}
            >
              <View style={styles.genderSubContainer}>
                <Text style={styles.genderText}>
                  {language.en.specifyGender}
                </Text>
                <RadioGroup
                  style={styles.radioGroup}
                  selectedIndex={
                    this.state.selectedIndex == null
                      ? -1
                      : this.state.selectedIndex
                  }
                  onSelect={(index, value) => this.onSelect(index, value)}
                >
                  <RadioButton
                    value={language.en.male}
                    selectedCheckbox={require('../../assets/images/gender_check.png')}
                    unSelectedCheckbox={require('../../assets/images/gender_uncheck.png')}
                    checkBoxLeft={true}
                  >
                    <Text style={styles.genderMale}>{language.en.male}</Text>
                  </RadioButton>
                  <RadioButton
                    value={language.en.female}
                    selectedCheckbox={require('../../assets/images/gender_check.png')}
                    unSelectedCheckbox={require('../../assets/images/gender_uncheck.png')}
                    checkBoxLeft={true}
                  >
                    <Text style={styles.genderFemale}>
                      {language.en.female}
                    </Text>
                  </RadioButton>
                  <RadioButton
                    value={language.en.nonBinary}
                    selectedCheckbox={require('../../assets/images/gender_check.png')}
                    unSelectedCheckbox={require('../../assets/images/gender_uncheck.png')}
                    checkBoxLeft={true}
                  >
                    <Text style={styles.noBinary}>{language.en.nonBinary}</Text>
                  </RadioButton>
                </RadioGroup>
              </View>
            </View>
          </Modal>
          <CustomModal
            onPressOkButton={() =>
              this.setState({
                showModal: false,
              })
            }
            showDialog={this.state.showModal}
            message={
              this.state.customMessage
                ? language.en.genderError
                : this.getErrorMessages()
            }
          />
          <CustomModal
            onPressOkButton={() => onHideSignUpErrorDialog()}
            showDialog={signUp.showSignUpErrorDialog}
            message={signUp.errorMessage}
          />
          <CustomModal
            onPressOkButton={() => {
              onHideSignUpSuccessDialog();
              navigation.goBack();
            }}
            showDialog={signUp.showSignUpSuccessDialog}
            message={
              signUp.signUpResponse != null
                ? signUp.signUpResponse.data.result
                : ''
            }
          />
          <TouchableOpacity
            style={styles.backButton}
            onPress={() => navigation.goBack()}
          >
            <Icon size={50} color="white" name="ios-arrow-round-back" />
          </TouchableOpacity>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  addButtonView: {
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 35,
    height: 36,
    justifyContent: 'center',
    position: 'absolute',
    right: 0,
    top: 10,
    width: 36,
  },
  addIcon: {position: 'absolute'},
  alreadyHaveAccountView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginBottom: 10,
  },
  alreadyHaveText: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 15,
  },
  backButton: {
    alignItems: 'center',
    height: 70,
    justifyContent: 'center',
    marginTop: 20,
    position: 'absolute',
    width: 70,
  },
  bottomBorder: {
    backgroundColor: 'white',
    height: 0.5,
    width: Metrics.DEVICE_WIDTH - 40,
  },
  chooseGenderView: {flex: 1, height: 50, marginLeft: 10},
  choosePicContainer: {flexWrap: 'wrap', marginTop: 40},
  choosePicView: {
    alignItems: 'center',
    flex: 1,
    height: Metrics.DEVICE_HEIGHT * 0.4,
    justifyContent: 'center',
  },
  chooseWorkoutView: {flex: 1, height: 50, marginLeft: 10},
  container: {
    flex: 1,
  },
  currentAddressContainer: {flex: 1, justifyContent: 'center'},
  currentAddressSubContainer: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  currentAddressText: {color: 'white', fontSize: 15,paddingBottom: 5,paddingTop: 5},
  currentAddressView: {flex: 1, height: 50, marginLeft: 10},
  fieldInput: {
    color: 'white',
    flex: 1,
    fontSize: 15,
    height: 45,
    marginTop: 15,
    marginLeft: 10,
  },
  fieldsView: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  frameView: {
    height: Metrics.DEVICE_HEIGHT,
    position: 'absolute',
    width: Metrics.DEVICE_WIDTH,
  },
  genderContainer: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 5,
  },
  genderFemale: {color: 'black', fontSize: 15},
  genderMale: {color: 'black', fontSize: 15},
  genderModalView: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  genderSubContainer: {
    backgroundColor: 'white',
    borderRadius: 20,
    flexWrap: 'wrap',
    justifyContent: 'center',
    position: 'absolute',
    width: Metrics.DEVICE_WIDTH * 0.6,
  },
  genderText: {
    alignSelf: 'center',
    color: 'black',
    fontSize: 17,
    marginBottom: 20,
    marginTop: 10,
  },
  imageBackground: {flex: 1},
  indicatorStyle: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    position: 'absolute',
  },
  inputStyle: {
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    marginTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
  },
  noBinary: {color: 'black', fontSize: 15},
  radioGroup: {paddingBottom: 10},
  scrollView: {flex: 1},
  selectGenderText: {color: 'white', fontSize: 16},
  signIn: {
    color: Color.colorPrimary,
    fontSize: 15,
    marginLeft: 5,
  },
  signInText: {alignSelf: 'center'},
  signUpButton: {
    flexWrap: 'wrap',
    marginBottom: 20,
    marginTop: 30,
    width: Metrics.DEVICE_WIDTH * 0.65,
  },
  workoutContainer: {flex: 1, justifyContent: 'center'},
  workoutSubContainer: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  workoutText: {color: 'white', fontSize: 15},
});

const mapStateToProps = (state) => {
  return {
    login: state.login,
    signUp: state.signUp,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    clearFieldsData: bindActionCreators(clearSignUpFieldsData, dispatch),
    onClickSignUp: bindActionCreators(onSignUp, dispatch),
    onHideSignUpErrorDialog: bindActionCreators(hideSignUpErrorDialog, dispatch),
    onHideSignUpSuccessDialog: bindActionCreators(hideSignUpSuccessDialog, dispatch)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUpScreen);
