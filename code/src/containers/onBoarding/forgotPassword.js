import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Image,
  Platform,
  TextInput,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {bindActionCreators} from 'redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import ValidationComponent from 'react-native-form-validator';
import {connect} from 'react-redux';
import IonIcon from 'react-native-vector-icons/Ionicons';
import Metrics from '../../config/metrics';
import {language} from '../../config/localization';
import {CustomButton} from '../../components/common/customButton';
import {CustomModal} from '../../components/common/customModal';
import {
  hideSuccessDialog,
  hideErrorDialog,
  onResetPassword,
} from '../../actions/forgot-password-types';

class ForgotPasswordScreen extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      showModal: false,
    };
  }

  static navigationOptions = {
    header: null,
  };

  onPressSubmit() {
    const {onClickSubmit} = this.props;

    const validate = this.validate({
      email: {email: true, required: true},
    });

    if (validate) {
      onClickSubmit(this.state.email);
    } else {
      this.setState({showModal: true});
    }
  }

  render() {
    const {navigation,forgotPassword,onHideSuccessDialog,onHideErrorDialog} = this.props;

    return (
      <View style={styles.container}>
        <ImageBackground
          style={styles.imageBackground}
          resizeMode={'stretch'}
          source={require('../../assets/images/screen_blur_bg.png')}
        >
          <ScrollView
            keyboardShouldPersistTaps={'handled'}
            showsVerticalScrollIndicator={false}
            style={styles.scrollView}
          >
            <View style={styles.appIconView}>
              <Image
                style={styles.appIconImage}
                resizeMode={'contain'}
                source={require('../../assets/images/ic_login.png')}
              />
              <View style={styles.forgotView}>
                <View style={styles.forgotContainer}>
                  <Text style={styles.forgotPasswordText}>
                    {language.en.forgotYourPassword}
                  </Text>
                </View>
                <View style={styles.forgotPasswordSubContainer}>
                  <Text style={styles.instructionLine}>
                    {language.en.instructionLine}
                  </Text>
                </View>

                <View style={styles.emailView}>
                  <Icon name="email" size={25} color="white" />
                  <TextInput
                    style={styles.emailInput}
                    ref={(email) => {this.email = email;}}
                    placeholder={'Email'}
                    value={this.state.email}
                    keyboardType={'email-address'}
                    selectionColor={'white'}
                    returnKeyType={'next'}
                    placeholderTextColor={'white'}
                    onChangeText={(email) => this.setState({email})}
                    underlineColorAndroid={'transparent'}
                  />
                </View>
                <View style={styles.bottomBorder} />
                <CustomButton
                  style={styles.submitButton}
                  showIcon={false}
                  btnText={language.en.submit}
                  onPress={() => {
                    this.onPressSubmit();
                  }}
                />
              </View>
            </View>
          </ScrollView>
        </ImageBackground>
        {Platform.OS === 'ios' ? <KeyboardSpacer /> : null}
        <CustomModal
          onPressOkButton={() =>
            this.setState({
              showModal: false,
            })
          }
          showDialog={this.state.showModal}
          message={this.getErrorMessages()}
        />
        <CustomModal
          onPressOkButton={() => onHideErrorDialog()}
          showDialog={forgotPassword.showErrorDialog}
          message={forgotPassword.errorMessage}
        />
        <CustomModal
          onPressOkButton={() => {
            onHideSuccessDialog();
            navigation.goBack();
          }}
          showDialog={forgotPassword.showSuccessDialog}
          message={forgotPassword.successMessage}
        />
        <TouchableOpacity
          style={styles.backButton}
          onPress={() => navigation.goBack()}
        >
          <IonIcon size={50} color="white" name="ios-arrow-round-back" />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  appIconImage: {
    height: Metrics.DEVICE_HEIGHT * 0.3,
    width: Metrics.DEVICE_WIDTH * 0.4,
  },
  appIconView: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: Metrics.DEVICE_HEIGHT * 0.1,
  },
  backButton: {
    alignItems: 'center',
    height: 70,
    justifyContent: 'center',
    marginTop: 20,
    position: 'absolute',
    width: 70,
  },
  bottomBorder: {
    backgroundColor: 'white',
    height: 0.5,
    width: Metrics.DEVICE_WIDTH - 40,
  },
  container: {
    flex: 1,
  },
  emailInput: {
    color: 'white',
    flex: 1,
    height: 45,
    marginTop: 15,
    marginLeft: 10,
  },
  emailView: {
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    marginTop: 30,
    paddingLeft: 10,
    paddingRight: 10,
  },
  forgotContainer: {
    alignItems: 'center',
    height: 30,
    justifyContent: 'center',
    marginTop: 20,
  },
  forgotPasswordSubContainer: {
    alignContent: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  forgotPasswordText: {color: 'white', fontSize: 17},
  forgotView: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    height: Metrics.DEVICE_HEIGHT * 0.6,
    justifyContent: 'center',
  },
  imageBackground: {flex: 1},
  instructionLine: {
    color: 'white',
    fontSize: 15,
    justifyContent: 'center',
    textAlign: 'center',
  },
  scrollView: {flex: 1},
  submitButton: {
    flexWrap: 'wrap',
    marginTop: 40,
    width: Metrics.DEVICE_WIDTH * 0.65,
  },
});

const mapStateToProps = (state) => {
  return {
    forgotPassword: state.forgotPassword,
  };
};

const mapDispatchToProps = (dispatch)=>(
  {
    onClickSubmit: bindActionCreators(onResetPassword, dispatch),
    onHideErrorDialog: bindActionCreators(hideErrorDialog, dispatch),
    onHideSuccessDialog: bindActionCreators(hideSuccessDialog, dispatch),
  });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ForgotPasswordScreen);
