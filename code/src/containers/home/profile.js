import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  NativeModules,
  TextInput,
  Modal,
  Platform,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import EvilIcon from 'react-native-vector-icons/EvilIcons';
import MaterialCommIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import FoundationIcon from 'react-native-vector-icons/Foundation';
import {Avatar} from 'react-native-elements';
import {connect} from 'react-redux';
import Metrics from '../../config/metrics';
import {language} from '../../config/localization';
import {DrawerNavBar} from '../../components/common/drawerNavBar';
const ImagePicker = NativeModules.ImageCropPicker;

class ProfileScreen extends Component {
  static navigationOptions = {
    header: (props) => (
      <DrawerNavBar navigation={props.navigation} title={language.en.spotter} />
    ),
  };

  constructor(props) {
    super(props);
    const {profileData} = this.props.userDetail;

    this.state = {
      aboutMe: '',
      firstName: profileData.firstname,
      gender: 'Male',
      genderModal: false,
      imagePath: '',
      lastName: profileData.lastname,
      selectedIndex: 0,
    };
  }

  pickSingle() {
    ImagePicker.openPicker({
      compressImageMaxHeight: 480,
      compressImageMaxWidth: 640,
      compressImageQuality: 0.5,
      compressVideoPreset: 'MediumQuality',
      cropperCircleOverlay: true,
      cropping: true,
      height: 300,
      includeExif: true,
      width: 300,
    })
      .then((image) => {
        this.setState({imagePath: image.path});
      })
      .catch((e) => {
        console.log(e);
        alert(e.message ? e.message : e);
      });
  }

  onSelect(index, value) {
    this.setState({gender: value,genderModal: false,selectedIndex: index});
  }

  render() {
    const {profileData} = this.props.userDetail;

    return (
      <View style={styles.container}>
        <ImageBackground
          style={styles.imageBackground}
          resizeMode={'stretch'}
          source={require('../../assets/images/screen_blur_bg.png')}
        >
          <ScrollView
            keyboardShouldPersistTaps={'handled'}
            showsVerticalScrollIndicator={false}
            style={styles.scrollView}
          >
            <View style={styles.subContainer}>
              <View style={styles.profileView}>
                {this.state.imagePath === '' ? (
                  <Avatar
                    xlarge
                    rounded
                    source={
                      profileData == null
                        ? require('../../assets/images/user_image.png')
                        : profileData.imageUrl === ''
                          ? require('../../assets/images/user_image.png')
                          : {
                            uri: profileData
                              .imageUrl,
                          }
                    }
                    activeOpacity={0.7}
                  />
                ) : (
                  <Avatar
                    xlarge
                    rounded
                    source={{uri: this.state.imagePath}}
                    activeOpacity={0.7}
                  />
                )}
                <TouchableOpacity
                  style={styles.addIconContainer}
                  onPress={() => this.pickSingle()}
                >
                  <Icon
                    size={30}
                    color="grey"
                    name="ios-add"
                    style={styles.choosePhotoIcon}
                  />
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                style={styles.chooseLocationContainer}
              >
                <EvilIcon name="location" size={30} color="white" />
                <View style={styles.locationTextContainer}>
                  <Text style={styles.locationText}>
                    {profileData.address}
                  </Text>
                </View>
              </TouchableOpacity>
              <TextInput
                style={styles.aboutMeInput}
                editable={false}
                value={this.state.aboutMe}
                placeholder={'Describe yourself in few adjectives'}
                keyboardType={'default'}
                selectionColor={'white'}
                returnKeyType={'done'}
                underlineColorAndroid={'transparent'}
                placeholderTextColor={'white'}
                onChangeText={(aboutMe) => this.setState({aboutMe})}
              />

              <View style={styles.fieldStyle}>
                <EntypoIcon name="edit" size={25} color="white" />
                <TextInput
                  style={styles.firstNameInput}
                  selectTextOnFocus={false}
                  ref={(firstName) => {
                    this.firstName = firstName;
                  }}
                  editable={false}
                  value={this.state.firstName}
                  keyboardType={'default'}
                  selectionColor={'white'}
                  returnKeyType={'done'}
                  placeholderTextColor={'white'}
                  maxLength={42}
                  onChangeText={(firstName) =>
                  {
                    this.setState({firstName});}
                  }
                  underlineColorAndroid={'transparent'}
                />
              </View>
              <View style={styles.bottomBorder} />
              <View style={styles.fieldStyle}>
                <EntypoIcon name="edit" size={25} color="white" />
                <TextInput
                  style={styles.lastNameInput}
                  ref={(lastName) => {
                    this.lastName = lastName;
                  }}
                  editable={false}
                  selectTextOnFocus={false}
                  value={this.state.lastName}
                  keyboardType={'default'}
                  selectionColor={'white'}
                  returnKeyType={'done'}
                  placeholderTextColor={'white'}
                  maxLength={42}
                  underlineColorAndroid={'transparent'}
                  onChangeText={(lastName) =>
                  {
                    this.setState({lastName});}
                  }
                />
              </View>
              <View style={styles.bottomBorder} />
              <View style={styles.fieldStyle}>
                <FoundationIcon name="male-female" size={25} color="white" />
                <TouchableOpacity
                  style={styles.genderContainer}
                  onPress={() =>
                    this.setState({genderModal: !this.state.genderModal})
                  }
                >
                  <View style={styles.genderSubContainer}>
                    <Text style={styles.gender}>
                      {this.state.gender}
                    </Text>
                    <Icon
                      name="ios-arrow-forward-outline"
                      size={25}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={styles.bottomBorder} />
              </View>
              <View style={styles.fieldStyle}>
                <MaterialCommIcon name="dumbbell" size={25} color="white" />
                <View style={styles.placeContainer}>
                  <TouchableOpacity
                    style={styles.placeSubContainer}
                  >
                    <View style={styles.placeTextContainer}>
                      <Text style={styles.placeNameText}>
                        {
                          'Ozi Gym'
                        }
                      </Text>
                      <Icon
                        name="ios-arrow-forward-outline"
                        size={25}
                        color="white"
                      />
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={styles.bottomBorder} />
              </View>
            </View>
          </ScrollView>
          {Platform.OS === 'ios' ? <KeyboardSpacer /> : null}
          <Modal
            transparent={true}
            visible={this.state.genderModal}
            animationType={'fade'}
            onRequestClose={() => {
              alert('Modal has been closed.');
            }}
          >
            <View
              style={styles.changeGenderContainer}
              onPress={() => {
                this.setState({genderModal: false});
              }}
            >
              <View style={styles.modalContainer}>
                <Text style={styles.genderText}>
                  {language.en.specifyGender}
                </Text>
                <RadioGroup
                  style={styles.radioGroup}
                  selectedIndex={this.state.selectedIndex}
                  onSelect={(index, value) => this.onSelect(index, value)}
                >
                  <RadioButton
                    value={language.en.male}
                    selectedCheckbox={require('../../assets/images/gender_check.png')}
                    unSelectedCheckbox={require('../../assets/images/gender_uncheck.png')}
                    checkBoxLeft={true}
                  >
                    <Text style={styles.optionsText}>{language.en.male}</Text>
                  </RadioButton>
                  <RadioButton
                    value={language.en.female}
                    selectedCheckbox={require('../../assets/images/gender_check.png')}
                    unSelectedCheckbox={require('../../assets/images/gender_uncheck.png')}
                    checkBoxLeft={true}
                  >
                    <Text style={styles.optionsText}>{language.en.female}</Text>
                  </RadioButton>
                  <RadioButton
                    value={language.en.nonBinary}
                    selectedCheckbox={require('../../assets/images/gender_check.png')}
                    unSelectedCheckbox={require('../../assets/images/gender_uncheck.png')}
                    checkBoxLeft={true}
                  >
                    <Text style={styles.optionsText}>
                      {language.en.nonBinary}
                    </Text>
                  </RadioButton>
                </RadioGroup>
              </View>
            </View>
          </Modal>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  aboutMeInput: {
    borderColor: 'white',
    borderRadius: 10,
    borderWidth: 2,
    color: 'white',
    height: 45,
    marginBottom: 20,
    marginTop: 30,
    padding: 10,
    width: Metrics.DEVICE_WIDTH * 0.7,
  },
  addIconContainer: {
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 35,
    height: 36,
    justifyContent: 'center',
    position: 'absolute',
    right: 0,
    top: 10,
    width: 36,
  },
  bottomBorder: {
    backgroundColor: 'white',
    height: 0.5,
    width: Metrics.DEVICE_WIDTH - 40,
  },
  changeGenderContainer: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  chooseLocationContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    marginTop: 40,
  },
  choosePhotoIcon: {position: 'absolute'},
  container: {
    flex: 1,
  },
  fieldStyle: {
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    marginTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
  },
  firstNameInput: {
    color: 'white',
    flex: 1,
    height: 45,
    marginLeft: 10,
  },
  frameView: {
    flex: 1,
    position: 'absolute',
  },
  gender: {color: 'white', fontSize: 16},
  genderContainer: {flex: 1, height: 50, marginLeft: 5},
  genderSubContainer: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 10,
  },
  genderText: {
    alignSelf: 'center',
    color: 'black',
    fontSize: 17,
    marginBottom: 20,
    marginTop: 10,
  },
  imageBackground: {flex: 1},
  lastNameInput: {
    color: 'white',
    flex: 1,
    height: 45,
    marginLeft: 10,
  },
  locationText: {
    color: 'white',
    fontSize: 16,
    paddingLeft: 10,
    paddingRight: 10,
    textAlign: 'center',
  },
  locationTextContainer: {flexWrap: 'wrap'},
  modalContainer: {
    backgroundColor: 'white',
    borderRadius: 20,
    flexWrap: 'wrap',
    justifyContent: 'center',
    position: 'absolute',
    width: Metrics.DEVICE_WIDTH * 0.6,
  },
  optionsText: {color: 'black', fontSize: 15},
  placeContainer: {flex: 1, height: 50, marginLeft: 10},
  placeNameText: {color: 'white', fontSize: 16},
  placeSubContainer: {flex: 1, justifyContent: 'center'},
  placeTextContainer: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  profileView: {flex: 1, marginTop: 40},
  radioGroup: {paddingBottom: 10},
  scrollView: {flex: 1},
  subContainer: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    paddingBottom: 45,
  },
});

const mapStateToProps = (state) => {
  return {
    userDetail: state.userDetail,
  };
};

export default connect(
  mapStateToProps
)(ProfileScreen);
