import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {language} from '../../config/localization';
import Metrics from '../../config/metrics';
import {CustomMapView} from '../../components/common/customMapView';
import {DrawerNavBar} from '../../components/common/drawerNavBar';
import {getUserProfile} from '../../actions/user-detail-action-types';
import {showLoader, hideLoader} from '../../actions/app-action-types';

class LocationScreen extends Component {

  static navigationOptions = {
    header: (props) => (
      <DrawerNavBar navigation={props.navigation} title={language.en.spotter} />
    ),
  };

  componentDidMount = async() => {
    const {hideProfileLoader,navigation,showProfileFetchLoader,userDetail,fetchUserProfile} = this.props;

    if (userDetail.profileData == null) {
      showProfileFetchLoader();
      try {
        const userData = await AsyncStorage.getItem('userData');

        if (userData != null) {
          fetchUserProfile(JSON.parse(userData),navigation);
          hideProfileLoader();
        }
      } catch (error) {
        hideProfileLoader();
        console.log(error);
      }
    }
    AsyncStorage.setItem('initialStack', 'DrawerStack');
  }

  renderOnMapReady() {
    return (
      <View style={styles.container}>
        {CustomMapView()}
        <View style={styles.markerContainer}>
          <View style={styles.markerSubContainer}>
            <TouchableOpacity
              style={styles.marker}
            >
              <Text style={styles.locationText}>{language.en.locations}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.friendsTab}
            >
              <Text style={styles.friendsText}>{language.en.friends}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.recentTab}
            >
              <Text style={styles.recentText}>{language.en.recent}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

  render() {
    return this.renderOnMapReady();
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  friendsTab: {
    alignItems: 'center',
    height: 45,
    justifyContent: 'center',
    width: Metrics.DEVICE_WIDTH * 0.25,
  },
  friendsText: {
    color: 'black',
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  locationText: {
    color: 'black',
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  marker: {
    alignItems: 'center',
    height: 45,
    justifyContent: 'center',
    width: Metrics.DEVICE_WIDTH * 0.25,
  },
  markerContainer: {
    alignSelf: 'center',
    bottom: Metrics.DEVICE_HEIGHT * 0.1,
    flexWrap: 'wrap',
    position: 'absolute',
  },
  markerSubContainer: {
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 45,
    flexDirection: 'row',
    height: 45,
    justifyContent: 'center',
    width: Metrics.DEVICE_WIDTH * 0.75,
  },
  recentTab: {
    alignItems: 'center',
    height: 45,
    justifyContent: 'center',
    width: Metrics.DEVICE_WIDTH * 0.25,
  },
  recentText: {
    color: 'black',
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
    textAlignVertical: 'center',
  },
});

const mapStateToProps = (state) =>(
  {
    userDetail: state.userDetail,
  }
);

const mapDispatchToProps = (dispatch)=>(
  {
    fetchUserProfile: bindActionCreators(getUserProfile, dispatch),
    hideProfileLoader: bindActionCreators(hideLoader, dispatch),
    showProfileFetchLoader: bindActionCreators(showLoader, dispatch),
  });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LocationScreen);
