import {Component} from 'react';
const BaseUrl = 'https://web7.staging02.com/spotter/';

export default class WebApi extends Component {
  static getApiHit(url, params, token) {
    return fetch(BaseUrl + url, {
      body: params,
      headers: {Authorization: `Bearer ${token}`},
      method: 'GET',
    })
      .then((response) => response.json())
      .then((responseJson) => {
        return responseJson;
      })
      .catch((error) => {
        console.log('Api Error', error);
        return false;
      });
  }

  static postApiWithRawData(url, params) {
    const formBody = JSON.stringify(params);

    return fetch(BaseUrl + url, {
      body: formBody,
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      method: 'POST',
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('Api response', JSON.stringify(responseJson));
        return responseJson;
      })
      .catch((error) => {
        console.log('Api Error', error);
        return false;
      });
  }

  static postApiHit(url, params) {
    const formData = new FormData();

    for (const property in params) {
      formData.append(property, params[property]);
    }
    return fetch(BaseUrl + url, {
      body: formData,
      headers: {Accept: 'application/json'},
      method: 'POST',
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('Api response', JSON.stringify(responseJson));
        return responseJson;
      })
      .catch((error) => {
        console.log('Api Error', error);
        return false;
      });
  }

  static postRawApiHitWithHeader(url, params, token) {
    return fetch(BaseUrl + url, {
      body: JSON.stringify(params),
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      method: 'POST',
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('Api response', JSON.stringify(responseJson));
        return responseJson;
      })
      .catch((error) => {
        console.log('Api Error', error);
        return false;
      });
  }

  static postApiHitWithHeader(url, params, token) {
    const formData = new FormData();

    for (const property in params) {
      formData.append(property, params[property]);
    }
    return fetch(BaseUrl + url, {
      body: formData,
      headers: {Authorization: `Bearer ${token}`},
      method: 'POST',
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('Api response', JSON.stringify(responseJson));
        return responseJson;
      })
      .catch((error) => {
        console.log('Api Error', error);
        return false;
      });
  }

  static getBase64(input) {
    const keyStr =
      'ABCDEFGHIJKLMNOP' +
      'QRSTUVWXYZabcdef' +
      'ghijklmnopqrstuv' +
      'wxyz0123456789+/' +
      '=';
    let output = '';
    let chr1,
      chr2,
      chr3 = '';
    let enc1,
      enc2,
      enc3,
      enc4 = '';
    let i = 0;

    do {
      chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);

      enc1 = chr1 > 2;
      enc2 = ((chr1 && 3) < 4) || (chr2 > 4);
      enc3 = ((chr2 && 15) < 2) || (chr3 > 6);
      enc4 = chr3 && 63;

      if (isNaN(chr2)) {
        enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
        enc4 = 64;
      }

      output =
        output +
        keyStr.charAt(enc1) +
        keyStr.charAt(enc2) +
        keyStr.charAt(enc3) +
        keyStr.charAt(enc4);
      chr1 = chr2 = chr3 = '';
      enc1 = enc2 = enc3 = enc4 = '';
    } while (i < input.length);
    return output;
  }
}
