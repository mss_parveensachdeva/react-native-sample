import React from 'react';
import {
  Animated,
  Easing,
} from 'react-native';
import {StackNavigator, DrawerNavigator} from 'react-navigation';
import LocationScreen from '../containers/home/location';
import {DrawerNavBar} from '../components/common/drawerNavBar';
import SignUpScreen from '../containers/onBoarding/signUp';
import ForgotPasswordScreen from '../containers/onBoarding/forgotPassword';
import LoginScreen from '../containers/onBoarding/login';
import {language} from '../config/localization';
import Metrics from '../config/metrics';
import DrawerMenu from '../components/common/drawerMenu';
import ProfileScreen from '../containers/home/profile';

const transitionConfig = () => {
  return {
    screenInterpolator: (sceneProps) => {
      const {layout, position, scene} = sceneProps;

      const thisSceneIndex = scene.index;
      const width = layout.initWidth;

      const translateX = position.interpolate({
        inputRange: [thisSceneIndex - 1, thisSceneIndex],
        outputRange: [width, 0],
      });

      return {transform: [{translateX}]};
    },
    transitionSpec: {
      duration: 500,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
      useNativeDriver: true,
    },
  };
};
// drawer stack
const DrawerStack = DrawerNavigator(
  {
    DrawerLocationScreen: {screen: LocationScreen},
    ProfileScreen: {screen: ProfileScreen},
  },
  {
    contentComponent: DrawerMenu,
    drawerWidth: Metrics.DEVICE_WIDTH * 0.7,
    transitionConfig: transitionConfig,
  }
);
const DrawerNavigation = StackNavigator(
  {
    AppDrawerStack: {screen: DrawerStack},
  },
  {
    headerMode: 'none',
    navigationOptions: ({navigation}) => ({
      header: <DrawerNavBar navigation={navigation} title={language.en.spotter} />,
    }),
  }
);
// login stack
const LoginStack = StackNavigator(
  {
    AddLoginScreen: {screen: LoginScreen},
    ForgotPasswordScreen: {screen: ForgotPasswordScreen},
    SignUpScreen: {screen: SignUpScreen},
  },
  {
    headerMode: 'none',
    navigationOptions: {
      gesturesEnabled: false,
    },
    transitionConfig: transitionConfig,
  }
);
const PrimaryNav = StackNavigator(
  {
    AppLoginStack: {screen: LoginStack},
    DrawerStack: {screen: DrawerNavigation},
  },
  {
    headerMode: 'screen',
    initialRouteName: 'AppLoginStack',
    navigationOptions: {
      drawerLockMode: 'locked-closed',
    },
    transitionConfig: transitionConfig,
  }
);

export default PrimaryNav;
