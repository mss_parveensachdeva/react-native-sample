import {SIGN_UP_SUCCESS, SIGN_UP_FAILURE,
  HIDE_SIGN_UP_SUCCESS_DIALOG, HIDE_SIGN_UP_ERROR_DIALOG, CLEAR_SIGN_UP_FIELD_DATA,
} from '../actions/sign-up-action-types';

const initialState = {
  errorMessage: '',
  showApiLoader: false,
  showSignUpErrorDialog: false,
  showSignUpSuccessDialog: false,
  signUpResponse: null,
};

export default function signUp(state = initialState, action) {
  switch (action.type) {
    case SIGN_UP_SUCCESS:
      return {
        ...state,
        showSignUpSuccessDialog: true,
        signUpResponse: action.signUpResponse,
      };
    case SIGN_UP_FAILURE:
      return {
        ...state,
        errorMessage: action.errorMessage,
        showSignUpErrorDialog: true,
      };
    case HIDE_SIGN_UP_SUCCESS_DIALOG:
      return {
        ...state,
        showSignUpSuccessDialog: false,
      };
    case HIDE_SIGN_UP_ERROR_DIALOG:
      return {
        ...state,
        showSignUpErrorDialog: false,
      };
    case CLEAR_SIGN_UP_FIELD_DATA:
      return {
        ...state,
        errorMessage: '',
        showApiLoader: false,
        showSignUpErrorDialog: false,
        showSignUpSuccessDialog: false,
        signUpResponse: null,
      };
    default:
      return state;
  }
}
