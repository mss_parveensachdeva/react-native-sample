import {FETCH_PROFILE_SUCCESS} from '../actions/user-detail-action-types';
import {CLEAR_USER_DETAIL_DATA} from '../actions/login-action-types';

const initialState = {
  profileData: null,
  userData: null,
};

export default function userDetail(state = initialState, action) {
  switch (action.type) {
    case FETCH_PROFILE_SUCCESS:
      return {
        ...state,
        profileData: action.profileData,
        userData: action.userData,
      };
    case CLEAR_USER_DETAIL_DATA:
      return {
        profileData: null,
        userData: null,
      };
    default:
      return state;
  }
}
