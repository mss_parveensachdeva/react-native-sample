import {combineReducers} from 'redux';
import login from './login';
import signUp from './signUp';
import app from './app';
import userDetail from './userDetail';
import forgotPassword from './forgotPassword';
const rootReducer = combineReducers({
  app,
  forgotPassword,
  login,
  signUp,
  userDetail,
});

export default rootReducer;
