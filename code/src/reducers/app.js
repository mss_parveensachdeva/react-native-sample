import {SHOW_LOADER, HIDE_LOADER} from '../actions/app-action-types';

const initialState = {
  showLoading: false,
};

export default function app(state = initialState, action) {
  switch (action.type) {
    case SHOW_LOADER:
      return {
        ...state,
        showLoading: true,
      };
    case HIDE_LOADER:
      return {
        ...state,
        showLoading: false,
      };
    default:
      return state;
  }
}
