import {LOGIN_SUCCESS, LOGIN_FAILURE, HIDE_LOGIN_ERROR_DIALOG} from '../actions/login-action-types';

const initialState = {
  currentAddress: 'Location',
  currentRegion: null,
  errorMessage: '',
  showErrorDialog: false,
};

export default function login(state = initialState, action) {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
      };
    case LOGIN_FAILURE:
      return {
        ...state,
        errorMessage: action.errorMessage,
        showErrorDialog: true,
      };
    case HIDE_LOGIN_ERROR_DIALOG:
      return {
        ...state,
        showErrorDialog: false,
      };
    default:
      return state;
  }
}
