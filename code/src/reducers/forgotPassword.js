import {PASSWORD_RESET_SUCCESS, PASSWORD_RESET_FAILURE, HIDE_SUCCESS_DIALOG, HIDE_ERROR_DIALOG} from '../actions/forgot-password-types';

const initialState = {
  errorMessage: '',
  showErrorDialog: false,
  showSuccessDialog: false,
  successMessage: '',
};

export default function forgotPassword(state = initialState, action) {
  switch (action.type) {
    case PASSWORD_RESET_SUCCESS:
      return {
        ...state,
        showSuccessDialog: true,
        successMessage: action.successMessage,
      };
    case PASSWORD_RESET_FAILURE:
      return {
        ...state,
        errorMessage: action.errorMessage,
        showErrorDialog: true,
      };
    case HIDE_SUCCESS_DIALOG:
      return {
        ...state,
        showSuccessDialog: false,
      };
    case HIDE_ERROR_DIALOG:
      return {
        ...state,
        showErrorDialog: false,
      };
    default:
      return state;
  }
}
