import React, {Component} from 'react';
import {connect} from 'react-redux';
import firebase from 'react-native-firebase';
import {StyleSheet, View, ActivityIndicator} from 'react-native';
import Navigator from  './src/config/navigator';
import {Color} from './src/constants/colors';
import Metrics from './src/config/metrics';

const styles = StyleSheet.create({
  appIndicator: {flex: 1},
  container: {flex: 1},
  indicatorView: {
    alignItems: 'center',
    height: Metrics.DEVICE_HEIGHT,
    justifyContent: 'center',
    position: 'absolute',
    width: Metrics.DEVICE_WIDTH,
  },
});

class App extends Component {
  componentDidMount(){
    console.disableYellowBox = true;
    firebase.analytics().setCurrentScreen('App Opened');
  }

  renderLoader = () => {
    if (this.props.app.showLoading) {
      return (
        <View style={styles.indicatorView}>
          <ActivityIndicator
            style={styles.appIndicator}
            color={Color.colorPrimary}
            size="small"
            animating={this.props.app.showLoading}
          />
        </View>
      );
    }
    return null;
  };

  render() {
    return (
      <View style={styles.container}>
        <Navigator />
        {this.renderLoader()}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {app: state.app};
};

export default connect(mapStateToProps)(App);
